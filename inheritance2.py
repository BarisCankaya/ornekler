class Ana:
    AnaAttr=100
    def __init__(self):
        print("Ana Constructor Çağrıldı")
    def AnaMethod(self):
        print("Ana Method Çağrıldı")

class Cocuk(Ana):
    def __init__(self):
        print("Cocuk Constructor çağrıldı")
    def CocukMethod(self):
        print("Cocuk Method Çağrıldı")

c=Cocuk()
c.CocukMethod()
c.AnaMethod()
