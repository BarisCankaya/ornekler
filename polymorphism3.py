class Calisan:
    def ekle(self,ad,soyad,maas):
        self.ad=ad
        self.soyad=soyad
        self.maas=maas
    def zamYap(self):
        return self.maas*1.25
class Stajyer(Calisan):
    def ekle(self,ad,soyad,maas):
        self.ad=ad
        self.soyad=soyad
        self.maas = maas
        print("Stajyer Adı Soyadı : {} {} {}".format(self.ad,self.soyad,self.maas))
    def zamYap(self):
        print(self.maas*1.15)
class Muhendis(Calisan):
    def ekle(self,ad,soyad,maas):
        self.ad=ad
        self.soyad=soyad
        self.maas = maas
        print("Mühendis Adı Soyadı : {} {} {} ".format(self.ad,self.soyad,self.maas))
    def zamYap(self):
        print(self.maas * 1.50)
class Ogrenci(Calisan):
    def ekle(self,ad,soyad,maas):
        self.ad=ad
        self.soyad=soyad
        self.maas=maas
        print("Öğrenci Adı Soyadı : {} {} {}".format(self.ad,self.soyad,self.maas))
    def zamYap(self):
        print(self.maas * 1)
def meslek(meslekTipi,ad,soyad,maas):
    meslekTipi.ekle(ad,soyad,maas)
s1=Stajyer()
m1=Muhendis()
o1=Ogrenci()

meslek(s1,"Barış","Çankaya",548)
meslek(m1,"Ali Haydar","Öztürk",3000)
meslek(o1,"Melih","Okumuşlar",100)
s1.zamYap()
m1.zamYap()
o1.zamYap()