metin="BARIŞ ÇANKAYA"
# ilk harfi büyük yapar
print(metin.capitalize())

# kelimelerin ilk harfini büyük yapar
print(metin.title())

# Belirtilen harfi string değerin içinde arar ve adedini veri
print(metin.count('A'))

# Tüm harfleri büyük yapar
print(metin.upper())

# Tüm harfleri küçük yapar
print(metin.lower())

# Harfler büyükse küçük, küçükse büyük yapar
print(metin.swapcase())

# Belirtildiği miktar kadar karakteri sağdan ve soldan boşluk bırakarak ortalar
print(metin.center(5))