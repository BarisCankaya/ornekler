from django.shortcuts import render
from myAPI.forms import UserForm
from myAPI.biz.userBO import UserBO
from django.contrib import messages
def create(request):
    form=UserForm(request.POST)
    userBO=UserBO()
    if request.method=="POST":
        if form.is_valid():
            form = UserForm(request.POST)
            height = request.POST.get('height')
            weight = request.POST.get('weight')
            shoeSize = request.POST.get('shoeSize')
            result=userBO.Control(height,weight,shoeSize)
            messages.success(request,result)


    else:
        form=UserForm()
    return render(request,'app/index.html',{'form':form})