# Generated by Django 2.2.7 on 2019-11-28 10:11

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('myAPI', '0006_auto_20191128_0854'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='users',
            name='name',
        ),
        migrations.RemoveField(
            model_name='users',
            name='surname',
        ),
    ]
