# Generated by Django 2.2.7 on 2019-11-28 05:29

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('myAPI', '0002_auto_20191127_1249'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='users',
            name='name',
        ),
        migrations.RemoveField(
            model_name='users',
            name='surname',
        ),
    ]
