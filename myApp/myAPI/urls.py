from django.conf.urls import url
from .views import create
from .views import UserList
urlpatterns=[
    url(r'^create/',create),
    url(r'^users/', UserList.as_view()),
]