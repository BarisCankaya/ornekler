if __name__ == "__main__":
    print("Eğer doğrudan çalıştırılmışsam beni görürsünüz.")
    print(__name__)

else:
    print("Beni yalnızca içe aktardığınızda görebilirsiniz.")
    print(__name__)