from tkinter import *
import math
def sayiEkle(sayi):
    label.config(text="")
    if textBox.get()=="" or textBox.get()=="0":
        textBox.delete(0,END)
        textBox.insert(END,sayi)
    else:
        textBox.insert(END,sayi)

def operator(a):
    if textBox.get()=="":
        pass
    else:
        textBox.insert(END,a)
    btnVirgul.config(state=NORMAL)
def virgulEkle():
    sayi1=textBox.get()
    if textBox.get()=="":
        sayi1="0."
    else:
        sayi1=sayi1+"."
    btnVirgul.config(state=DISABLED)
    textBox.delete(0,END)
    textBox.insert(0,sayi1)
def temizle():
    textBox.delete(0,END)
    label.config(text="")
    btnVirgul.config(state=NORMAL)
def hesapla():
    sayi1=textBox.get()
    sonuc=eval(sayi1)
    temizle()
    label.config(text=sonuc)
    btnVirgul.config(state=NORMAL)
def kokAl():
    sayi1=float(textBox.get())
    sonuc=math.sqrt(sayi1)
    temizle()
    textBox.insert(0,sonuc)
    btnVirgul.config(state=NORMAL)
pencere=Tk()
pencere.title("Hesap Makinesi")
pencere.geometry("270x400+300+300")
pencere.resizable(0,0)


frame1=Frame(pencere,bg="white")
frame1.pack(expand=True,fill="both")

textBox=Entry(frame1,font="Verdana 30 normal",justify=RIGHT)
textBox.pack(expand=True,fill="both",side=TOP)
label=Label(frame1,font="Verdana 18 normal",text="",justify=RIGHT,bg="white")
label.pack(fill="both",side=RIGHT)
frame2=Frame(pencere)
frame2.pack(expand=True,fill="both")
btn7=Button(frame2,text="7",font="Verdana 18 normal",width="1",command=lambda :sayiEkle(7),border="0")
btn7.pack(expand=True,fill="both",side=LEFT)
btn8=Button(frame2,text="8",font="Verdana 18 normal",width="1",command=lambda :sayiEkle(8),border="0")
btn8.pack(expand=True,fill="both",side=LEFT)
btn9=Button(frame2,text="9",font="Verdana 18 normal",width="1",command=lambda :sayiEkle(9),border="0")
btn9.pack(expand=True,fill="both",side=LEFT)
btnBol=Button(frame2,text="/",font="Verdana 18 normal",width="1",command=lambda :operator("/"),border="0")
btnBol.pack(expand=True,fill="both",side=LEFT)
btnMod=Button(frame2,text="%",font="Verdana 18 normal",width="1",command=lambda:operator("%"),border="0")
btnMod.pack(expand=True,fill="both",side=LEFT)

frame3=Frame(pencere)
frame3.pack(expand=True,fill="both")

btn4=Button(frame3,text="4",font="Verdana 18 normal",width="1",command=lambda :sayiEkle(4),border="0")
btn4.pack(expand=True,fill="both",side=LEFT)
btn5=Button(frame3,text="5",font="Verdana 18 normal",width="1",command=lambda :sayiEkle(5),border="0")
btn5.pack(expand=True,fill="both",side=LEFT)
btn6=Button(frame3,text="6",font="Verdana 18 normal",width="1",command=lambda :sayiEkle(6),border="0")
btn6.pack(expand=True,fill="both",side=LEFT)
btnCarp=Button(frame3,text="*",font="Verdana 18 normal",width="1",command=lambda :operator("*"),border="0")
btnCarp.pack(expand=True,fill="both",side=LEFT)
btnKok=Button(frame3,text="√",font="Verdana 18 normal",width="1",command=lambda:kokAl(),border="0")
btnKok.pack(expand=True,fill="both",side=LEFT)


frame4=Frame(pencere)
frame4.pack(expand=True,fill="both")

btn1=Button(frame4,text="1",font="Verdana 18 normal",width="1",command=lambda :sayiEkle(1),border="0")
btn1.pack(expand=True,fill="both",side=LEFT)
btn2=Button(frame4,text="2",font="Verdana 18 normal",width="1",command=lambda :sayiEkle(2),border="0")
btn2.pack(expand=True,fill="both",side=LEFT)
btn3=Button(frame4,text="3",font="Verdana 18 normal",width="1",command=lambda :sayiEkle(3),border="0")
btn3.pack(expand=True,fill="both",side=LEFT)
btnCikar=Button(frame4,text="-",font="Verdana 18 normal",width="1",command=lambda :operator("-"),border="0")
btnCikar.pack(expand=True,fill="both",side=LEFT)
btnC=Button(frame4,text="C",font="Verdana 18 normal",width="1",command=lambda :temizle(),border="0")
btnC.pack(expand=True,fill="both",side=LEFT)

frame5=Frame(pencere)
frame5.pack(expand=True,fill="both")

btn0=Button(frame5,text="0",font="Verdana 18 normal",width="5",command=lambda :sayiEkle(0),border="0")
btn0.pack(expand=True,fill="both",side=LEFT)
btnVirgul=Button(frame5,text=",",font="Verdana 18 normal",width="1",command=lambda :virgulEkle(),border="0")
btnVirgul.pack(expand=True,fill="both",side=LEFT)
btnTopla=Button(frame5,text="+",font="Verdana 18 normal",width="1",command=lambda :operator("+"),border="0")
btnTopla.pack(expand=True,fill="both",side=LEFT)
btnEsittir=Button(frame5,text="=",font="Verdana 18 normal",width="1",command=lambda:hesapla(),border="0")
btnEsittir.pack(expand=True,fill="both",side=LEFT)
pencere.mainloop()