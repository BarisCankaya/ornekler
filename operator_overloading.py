class Vektor:
    def __init__(self,x,y):
        self.x=x
        self.y=y
    def __str__(self):
        return 'Vektor (%d, %d)'%(self.x,self.y)
    def __add__(self, other):
        return Vektor(self.x+other.x,self.y+other.y)
v1=Vektor(2,10)
v2=Vektor(5,-2)
print(v1+v2)